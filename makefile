default_target: package

clean:
	@mvn clean

package: clean
	@mvn package

start: package
	@java -jar target/web-client.war

deployMathius: package
	@mvn tomcat7:redeploy-only -Dtarget=mathius-production

deployIUT: package
	@mvn tomcat7:redeploy-only -Dtarget=iut-production

deployMathiusTesting: package
	@mvn tomcat7:redeploy-only

deployIUTTesting: package
	@mvn tomcat7:redeploy-only -Dtarget=iut-develop