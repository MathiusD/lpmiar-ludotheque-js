# Web-Client

## Note en cas de Fork/Reprise

La ci est prévue pour déployer à la fois sur le serveur de l'iut de nantes et mon serveur personnel.
En ce sens on a à peu près 3 000 variables de CI que je document ci après.
Dans la ci sauf si vous souhaitez déployer sur un tomcat perso je vous déconseille fortement de conserver la section de déployement `mathius` (Dans la ci et dans le pom dcp).
D'ailleurs les runners gitlab de l'univ n'ont pas accès à central vous devez donc [pluger une ci externe](https://gitlab.com/projects/new#cicd_for_external_repo) ou [mettre en place un miroir](https://gitlab.com/projects/new#import_project) (Pour le dernier choix faut juste penser à cocher la case `Mirror repository`).
Petit détail, les liens sont sur `gitlab.com` mais si vous avez une instance perso vous pouvez aussi.

### Variables CI

#### Construction

##### IUT-Build

Pour gérer construire l'apk associés aux api de l'iut

* `API_DEVELOP_IUT` : Api de dev de l'iut
* `API_RELEASE_IUT` : Api de prod de l'iut

##### Perso-Build

Pour gérer le déployement automatique sur un tomcat perso (Si vous l'utilisez je vous conseille de revoir le pom et la ci)

* `API_DEVELOP_MATHIUS` : Api de dev perso
* `API_RELEASE_MATHIUS` : Api de prod perso

Si vous n'envisager pas de déployer sur un serveur personnel je vous conseille de supprimer les tâches *:mathius dans la ci

#### Deploiement

##### IUT

Pour gérer le déployement automatique sur le serveur de l'iut

* `IUTTomcat_PASS` : Mdp pour vous connecter au tomcat de l'iut
* `IUTTomcat_USER` : User pour vous connecter au tomcat de l'iut

##### VPN

Vous devrez gérer la connection au vpn de l'univ sinon votre runner gitlab n'aura pas accès au serveur de l'iut pour déployer

* `IUT_VPN` : Url du vpn de l'univ dans notre cas : `nomade.etu.univ-nantes.fr`
* `IUT_VPN_ROLE` : Role utilisé pour vous connecter au VPN dans notre cas : `IUT de Nantes`
* `IUT_VPN_USER` : Votre nom d'utilisateur pour vous connecter aux services de l'univ (ex : `E**C***A`)
* `IUT_VPN_MDP` : Votre mdp pour vous connecter aux services de l'univ

##### Perso

Pour gérer le déployement automatique sur un tomcat perso (Si vous l'utilisez je vous conseille de revoir le pom et la ci)

* `MathiusTomcat_PASS` : Mdp pour vous connecter à votre tomcat
* `MathiusTomcat_USER` : User pour vous connecter à votre tomcat

Si vous n'envisager pas de déployer sur un serveur personnel je vous conseille de supprimer les tâches *:mathius dans la ci

## Info Diverses

Pour lancer l'application:
    `npm start`
ou
    `yarn run start`
