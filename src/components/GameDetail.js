import React, { useEffect, useState } from "react";
import 'react-bootstrap/dist/react-bootstrap.min.js';
import '../App.css';
import { useParams } from "react-router";
import { getGameFromId } from "../api/ApiGames";

function GameDetail({}) {
    //Recuperation de mon id dans le link
    const idGame = useParams();
    //Ma liste de jeux
    const [game, setGame] = useState([]);
    //Constante si le chargement prend trop de temps
    const [loading, setLoading] = useState([]);

    useEffect(() => {
        setLoading(true);
        getGameFromId(idGame.id)
        .then(res => {
            setGame(res);
            setLoading(false);
        });
    },[]);// eslint-disable-line react-hooks/exhaustive-deps

    

    if(loading) {
        return <p>Loading game...</p>
    }
    
    return (
        <div className="App">
            <h1>{game.name}</h1>
            <ul>
                {game.description !== null ? <li> Description : {game.description} </li> : <li>Pas de description</li>}
                {game.author !== null ? <li> Auteur : {game.author} </li> : <li>Pas d'auteur</li>}
                {game.companyName !== null ?  <li> Editeur : {game.companyName} </li> : <li>Pas d'éditeur</li>}
                {game.type !== null ? <li> Jeu de type {game.type}</li> : <li>Pas de type</li>}
                {game.nbPlayer !== null ? <li>A partir de {game.nbPlayer.min} joueurs</li> : <li>Pas de nombre de joueurs</li>}
                {game.time !== null ? <li>Environ {game.time.averageTime} {game.time.timeUnit}</li> : <li>Pas de temps moyen</li>}
                {game.yearRecommended !== null ?  <li>A partir de {game.yearRecommended.min} ans</li> : <li>Pas d'âge minimum</li>}
            </ul>
        </div>
    )
}

export default GameDetail;
