import axios from "axios";
import lienApi from './lienApi.json'

const url = lienApi.url;

//#region Requêtes pour Games

export const getGames = () => {
    return axios
    .get(url + `games?schema=shortGame`)
    .then(res => res.data)
    .catch(err => console.log(err));
}

export const getTypes = () => {
    return axios
    .get(url + "games/types")
    .then(res => res.data)
    .catch(err => console.log(err));
}

export const getGameFromId = (id) => {
    console.log(url+"games/"+id);
    return axios
    .get(url + "games/" + id)
    .then(res => res.data)
    .catch(err => console.log(err));
}

export const getGamesWithFilter = (nextUrl) => {
    return axios
    .get(url + nextUrl)
    .then(res => res.data)
    .catch(err => console.log(err));
}

export const postBorrowGame = (id,token) => {
    return axios
    .get(url + "games/" + id +"/take", { headers: { token: token } })
    .catch(err => console.log(err));
}

//#endregion

//#region Requêtes pour Profil

export const getCurrentUserInformation = (token) => {
    return axios
    .get(url + "my", { headers: { token: token } })
    .then(res => res.data)
    .catch(err => console.log(err));
}

export const getLoanOfCurrentUser = (token) => {
    return axios
    .get(url + "my/loans", { headers: { token: token } })
    .then(res => res.data)
    .catch(err => console.log(err));
}

export const deleteLoanOfCurrentUser = (id,token) => {
    return axios
    .delete(url + "library/loans/" + id , { headers: { token: token } })
    .then(res => res.data)
    .catch(err => console.log(err));
}

//#endregion

//#region Requêtes pour connexion

export const getAllProvider = () => {
    return axios
    .get(url + "oauth/providers")
    .then(res => res.data)
    .catch(err => console.log(err));
}

export const login = (provider,key) => {
    return url + "login/"+provider+"?key="+key;
}

export const returnLogin = (key) => {
    return axios
    .get(url + "oauth/return/"+key)
    .then(res => res.data)
    .catch(err => console.log(err));
}


//#endregion