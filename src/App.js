import React, { useEffect, useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Accueil from "./views/Accueil";
import Catalogue from "./views/Catalogue";
import Connexion from "./views/Connexion";
import Profil from "./views/Profil";
import Deconnexion from "./views/Deconnexion";
import GameDetail from "./components/GameDetail";
import './views/style.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
}from "react-router-dom";

function BasicExample()  {

  const [token, settoken] = useState();
useEffect(() => {
    settoken(JSON.parse(sessionStorage.getItem("token")));  
}, []);

  return (
    <Router basename={process.env.REACT_APP_ROUTER_BASE || ''}>
    <div id="header">
      <div class="container-fluid bg-dark pb-5 base">
        <div class="navbar">
          <ul class="list-group list-group-horizontal-xl text-right">
            <li class="list-group-item bg-dark liNavbar">
              <Link to="/">Accueil</Link>
            </li>
            <li class="list-group-item bg-dark liNavbar">
              <Link to="/Catalogue">Catalogue</Link>
            </li>
            {token === null ? null
              :<li class="list-group-item bg-dark liNavbar">
              <Link to="/Profil">Profil</Link>
            </li>
            }
            {token === null ?       <li class="list-group-item bg-dark liNavbar">
              <Link to="/Connexion">Connexion</Link>
              </li>
              :<li class="list-group-item bg-dark liNavbar">
              <Link to="/Deconnexion">Déconnexion</Link>
              </li>
            }
          </ul>
        </div>
          <h1 class="text-center text-white pt-4">Ludothèque</h1>
        </div>

        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}
        <Switch>
          <Route exact path="/">
            <Accueil />
          </Route>
          <Route exact path="/Catalogue">
            <Catalogue />
          </Route>
          <Route path="/Catalogue/:id">
            <GameDetail />
          </Route>
          <Route path="/Connexion">
            <Connexion />
          </Route>
          <Route exact path="/Profil">
            <Profil />
          </Route>
          <Route exact path="/Deconnexion">
            <Deconnexion />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default BasicExample 