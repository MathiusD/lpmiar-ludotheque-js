import React, { useEffect, useState } from "react";
import { getAllProvider,login,returnLogin} from "../api/ApiGames";
import uuid from 'react-uuid'
import { Redirect } from "react-router-dom";

function Connexion() {

  const [provider, setProvider] = useState([]);
  const [randomKey, setRandomKey] = useState([uuid()]);

useEffect(() => {
  getAllProvider()
  .then((res) => {
    setProvider(res);
  })
}, []);

const connexion =(provider)=>{
 const tempWindow = window.open(  login(provider,randomKey), 'connexion','height=700,width=700');
    var timer = setInterval(function() {
        if (tempWindow.closed) {
          clearInterval(timer);
            returnLogin(randomKey)
            .then((res) => {
              sessionStorage.setItem("token", JSON.stringify(res.token));
              window.location = process.env.PUBLIC_URL + "/";
            })
            .catch(err => window.alert("connexion annuler"));
        }
    }, 500);

}
if( JSON.parse(sessionStorage.getItem("token")) === null){
    return (
  
      <div>
        <h1 style={{ textAlign: "center" }}>Connexion</h1>
        <p style={{ textAlign: "center" }}>Afin de vous connecter veuillez sélectionner un des formulaires.</p>
        <p style={{ textAlign: "center" }}>Compléter le puis fermer la fenêtre. </p>
        {provider.map(function(provider){
            return (
              <div className="jeu" style={{ textAlign: "center", paddingTop: '8px' }}>
                <input class="btn text-warning lienBoutonConnexion" type="button" value={"se connecter avec "+provider.nameProvider} onClick={() =>connexion(provider.nameProvider)}></input>
              </div>
            );
        })}
      </div>
    );}else{
      return <Redirect to="/"/>
    }
  }
  export default Connexion
