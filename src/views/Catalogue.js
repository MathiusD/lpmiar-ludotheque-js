import React, { useEffect, useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import Accueil from "./Accueil";
import { DropdownList, NumberPicker } from 'react-widgets';
import { Link, useHistory } from "react-router-dom";
import { getGamesWithFilter, postBorrowGame } from "../api/ApiGames";

function Catalogue() {

//#region Déclaration des constantes

  //Liste de jeux
  const [games, setGames] = useState([]);
  //Liste de jeux filtré
  const [filteredGames, setFilteredGames] = useState([]);
  //Ce que contient le champ de recherche
  const [search, setSearch] = useState("");
  //Remplace le await de js
  const [loading, setLoading] = useState([]);
  //Liste des genres
  const [types, setTypes] = useState([]);
  //Le type actuel
  const [selectedType, setSelectedType] = useState("all");
  const [selectedAge, setSelectedAge] = useState();
  const [selectedNbMinPlayer, setSelectedNbMinPlayer] = useState();
  const [available, setAvailable] = useState(true);
  const history = useHistory();
  
//#endregion

//#region useEffect

  useEffect(() => {
    setLoading(true);
    setGames(JSON.parse(sessionStorage.getItem("games")));
    setTypes(JSON.parse(sessionStorage.getItem("types")));
    setLoading(false);
  }, []);
    
  useEffect(() => {
    if(games !== null) {
      setFilteredGames(
        games.filter((game) =>
        game.name.toLowerCase().includes(search.toLowerCase())
        )
      )
      // if(available === true) {
      //   setFilteredGames(
      //     filteredGames.filter((game) =>
      //       game.gameState.isAvailable == true
      //     )
      //   )
      // }
    }
  }, [search, games, available]);

//#endregion

//#region Gestion de l'url pour la requête

  const searchGames = () => {
    var url = "games?schema=shortGame";
    if(selectedType !== "all"){
      url = url + `&type=${selectedType}`;
    }
    
    if(selectedAge !== undefined){
      url = url + `&minYear=${selectedAge}`;
    }
    if(selectedNbMinPlayer !== undefined){
      url = url + `&minNbPlayer=${selectedNbMinPlayer}`;
    }
    
    setLoading(true);
    console.log(url);
    getGamesWithFilter(url)
    .then(res => {
        setGames(res);
        setLoading(false);
    });
    
  };
  
//#endregion

  if (loading) {
    return <p>Loading games...</p>
  };
  
//#region Gestion du html et des composants

  if(sessionStorage.getItem("games") === null || sessionStorage.getItem("types") === null) {
    return(
      history.push("/"),
      <Accueil/>
    );
  } else {
    return (
      <div class="container-fluid">
        <h1>Le catalogue de jeux</h1>
        <div>
          <ul className="list-group list-group-horizontal">
            <li className="list-group-item list-group-item-dark">
              <input
              type="text"
              placeholder="Search the game"
              onChange={(e) => setSearch(e.target.value)}
              />
            </li>
            <li className="list-group-item list-group-item-dark">
              <select name="type_selected" id="type-select" value={selectedType} onChange={(e) => {setSelectedType(e.target.value)}}>
                <option value="all">Tous les types</option>
                {types.map((type, idx) => (
                  <option key={idx} value={type}>{type}</option>
                ))}
              </select>
            </li>
            <li>
              Âge minimum pour jouer : 
              <NumberPicker
                value = {selectedAge}
                defaultValue = {1}
                min = {0}
                max = {20}
                onChange = {(value) => {setSelectedAge(value)}}
              />
            </li>
            <li>
              Nombre de joueurs minimum :
              <NumberPicker
                value = {selectedNbMinPlayer}
                defaultValue = {1}
                min = {0}
                max = {20}
                onChange = {(value) => {setSelectedNbMinPlayer(value)}}
              />
            </li>            
            <button className="form-input-btn" type="submit" onClick={searchGames}>Rechercher</button>
          </ul>
        </div>
        <br/>
        <div class="container-fluid">
          <div className="row">
            {filteredGames.map((game, idx) => (
                    <GameDetail key={idx} {...game}/>
            ))}
          </div>
        </div>
      </div>
    )}
  }
  

  const GameDetail = (props) => {
    const {id, name, type, nbPlayer, time, yearRecommended, gameState} = props; 
    const [isDisponible, setDisponible] = useState(false);
    const [isConnected, setIsConnected] = useState(false);

    useEffect(() => {
      if(JSON.parse(sessionStorage.getItem("token"))) {
        setIsConnected(true);
      } else {
        setIsConnected(false);
      }
    }, [])
    
    
    useEffect(() => {
      setDisponible(gameState.isAvailable);
    }, [gameState]);
    
    
    const reservation = () => {
      const prevData = JSON.parse(sessionStorage.getItem("games"));
      prevData.forEach((element, key) => {
      if(element.id === id){
        element.gameState.isAvailable = false;
        setDisponible(false);
        postBorrowGame(id,JSON.parse(sessionStorage.getItem("token")));
        sessionStorage.setItem("games", JSON.stringify(prevData));
      }
    });

  };
    
    return (
      <div className="col-sm-6 box col-md-4 col-lg-2 box">
        <p key={id}>
        <Link to={`/Catalogue/${id}`}>{name}</Link>
        </p>
        {type !== null ? <p>Jeu de type {type}</p> : <p>Pas de type</p>}
        {nbPlayer !== null ? <p>A partir de {nbPlayer.min} joueurs</p> : <p>Pas de nombre de joueurs</p>}
        {time !== null ? <p>Environ {time.averageTime} {time.timeUnit}</p> : <p>Pas de temps moyen</p>}
        {yearRecommended !== null ?  <p>A partir de {yearRecommended.min} ans</p> : <p>Pas d'âge minimum</p>}
        {isConnected ? isDisponible ? <p><button type="submit" onClick={reservation}>reservation</button></p> : <p>Réservé</p> : null}
      </div>
      
    );
  }

//#endregion

export default Catalogue;
