import React, { useEffect, useState } from "react";
import { getGames, getTypes } from "../api/ApiGames";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
}from "react-router-dom";


function Accueil() {

  const [loading, setLoading] = useState([]);

  useEffect(() => {
    setLoading(true);
    if(sessionStorage.getItem("games") !== null){
      getGames()
      .then(res => {
        res.sort(function (a, b) {
          return a.name.localeCompare(b.name);
        });
        sessionStorage.setItem("games", JSON.stringify(res));
        setLoading(false);
      });
    }
    if(sessionStorage.getItem("types") !== null){
      getTypes()
      .then(res => {
          sessionStorage.setItem("types", JSON.stringify(res));
          setLoading(false);
      })
    }
    
  }, []);
  
  
  if (loading) {
    return <p>Loading games...</p>
  };

  return (
    <div class="container-fluid fond"style={{ 
      backgroundImage: `url(monopoly.jpg)` 
    }} >
      <div class="container accueil text-center align-self-center" >
        <p class="btn text-warning lienBouton"><Link to="/Catalogue">Visiter notre catalogue</Link></p>
      </div>
    </div>
  );
}
  export default Accueil