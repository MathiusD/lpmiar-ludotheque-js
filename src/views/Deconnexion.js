import React, { useEffect, useState } from "react";

function Deconnexion() {
        sessionStorage.setItem("token", null);
        window.location = process.env.PUBLIC_URL + "/";
}
export default Deconnexion