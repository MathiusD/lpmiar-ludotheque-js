import React, { useEffect, useState } from "react";
import { getCurrentUserInformation,getLoanOfCurrentUser,deleteLoanOfCurrentUser } from "../api/ApiGames";
import { Redirect } from "react-router-dom";
import '../App.css';

 function Profil() {

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [emprunts, setEmprunts] = useState([]);

  useEffect(() => {
    getCurrentUserInformation(JSON.parse(sessionStorage.getItem("token")))
    .then((res) => {
      setFirstName(res.firstName);
      setLastName(res.lastName);
    })
    .catch(err => window.alert("vous devez être authentifier pour accéder à cette page"));
  }, []);

  useEffect(() => {
    getLoanOfCurrentUser(JSON.parse(sessionStorage.getItem("token")))
    .then((res) => {
      setEmprunts(res);
    })
    .catch(err => window.alert("vous devez être authentifier pour accéder à cette page"));
  }, []);

    const empruntsRequested=[];
    const empruntsAtHome=[];
    const empruntsWaiting=[];
        emprunts.map(function(emprunt){
        if(emprunt.state.name === "Requested"){
          empruntsRequested.push(emprunt);
        }else if(emprunt.state.name === "At Home"){
          empruntsAtHome.push(emprunt);
        }else if(emprunt.state.name === "Waiting"){
          empruntsWaiting.push(emprunt);
        }
    })
    if( JSON.parse(sessionStorage.getItem("token")) != null){
    return (
      <div class="container">
        <div class="row offset-4 ">
          <div class="infoProfil text-center">
           <img src={process.env.PUBLIC_URL + "/utilisateur.png"} alt="Logo utilisateur" />
            <div class="text-left bold">
              <p><strong>nom :</strong> {lastName}</p>
              <p><strong>prenom :</strong> {firstName}</p> 
            </div>
          </div>
        </div>
        <h1>Mes emprunts en cours</h1>
        <div className="row">
        {empruntsAtHome.map((emprunt, idx) => (
                <EmpruntEnCours key={idx} {...emprunt}/>
              ))}
        </div>

        <h1>Mes emprunts en attente de récupération</h1>
        <div className="row">
        {empruntsWaiting.map((emprunt, idx) => (
                <EmpruntEnAttente key={idx} {...emprunt}/>
              ))}      
        </div>
        <h1>Mes emprunts en attente de validation</h1>
        <div className="row">
        {empruntsRequested.map((emprunt, idx) => (
                <EmpruntDemander key={idx} {...emprunt}/>
              ))}
        </div>
      </div>
    );
  }else{
    return <Redirect to="/"/>
  }
}

const EmpruntDemander = (props) => {
  const {id,game} = props; 

  const [isWaiting, setWaiting] = useState(true);
  const annulation =()=>{
    deleteLoanOfCurrentUser(id,JSON.parse(sessionStorage.getItem("token")));
    setWaiting(false);
    const prevData = JSON.parse(sessionStorage.getItem("games"));
      prevData.forEach((element, key) => {
      if(element.id === game.id){
        element.gameState.isAvailable = true;
        sessionStorage.setItem("games", JSON.stringify(prevData));
      }
    });
  }
  if(isWaiting){
  return (
    <div className="col-3 box">
      <p><strong>nom :</strong> {game.name}</p>
      <p><input type="button" value="annulation"onClick={annulation}></input></p>
    </div>
  );}
  else{return false;}
}

const EmpruntEnAttente = (props) => {
  const {game,startedAt,endAt} = props; 
  const timestampDebut = startedAt; // This would be the timestamp you want to format
  const dateDebut = new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit'}).format(timestampDebut);
  const timestampFin = endAt; // This would be the timestamp you want to format
  const dateFin = new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit'}).format(timestampFin);

  return (
    <div className="col-3 box">
      <p><strong>nom :</strong> {game.name}</p>
      <p><strong>date d'emprunt :</strong> {dateDebut}</p>
      <p><strong>date de retour prévu :</strong> {dateFin}</p>
    </div>
  );
}
const EmpruntEnCours = (props) => {
  const {game,startedAt,endAt} = props; 
  const timestampDebut = startedAt; // This would be the timestamp you want to format
  const dateDebut = new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit'}).format(timestampDebut);
  const timestampFin = endAt; // This would be the timestamp you want to format
  const dateFin = new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit'}).format(timestampFin);
return (
  <div className="col-3 box">
      <p><strong>nom :</strong> {game.name}</p>
      <p><strong>date d'emprunt :</strong> {dateDebut}</p>
      <p><strong>date de retour prévu :</strong> {dateFin}</p>
    </div>
  );
}


  export default Profil;